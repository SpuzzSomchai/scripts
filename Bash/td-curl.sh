#!/bin/bash

# Your login credentials
username="your_username"
password="your_password"

# URL for the login page
login_url="http://example.com/login"

# URL for the download
download_url="http://downloads.example.com/download/file.txt"

# Perform the login
login_response=$(curl -c cookies.txt -d "username=$username&password=$password" "$login_url")

# Check if the login was successful (you may need to adjust this condition)
if [[ $login_response == *"Login successful"* ]]; then
    echo "Login successful. Downloading file..."
    # Use the saved cookies to access the download URL
    download_response=$(curl -b cookies.txt "$download_url")
    
    # Save the downloaded file
    echo "$download_response" > /mnt/storage/downloads/downloaded_file.txt
    
    echo "Download complete."
else
    echo "Login failed."
fi
