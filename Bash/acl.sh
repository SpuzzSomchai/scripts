#! /bin/bash

Red="\e[31m"
Green="\e[32m"
Yellow="\e[33m"
Endcolor="\e[0m"

if [ $(id -u) -ne 0 ]; then echo "${Red}Please run as Root.{End}";
  exit 1;
  fi

  echo "${Yellow}For setting permissions similar to ACL in Windows. You will be prompted for a valid user name or group name first. Then you will enter the full path to the file or folder. For example "${Green}/folder/sub.${Endcolor}" ${Endofcolor}"
  echo "${Yellow} Persmission are set recurssively so make sure of your path. ${Endcolor}"
  echo "${Red} Enter user or group name: ${Endcolor}"
read name
  echo "${Red} Enter user name and path to directory or file: ${Endcolor}"
read path
	setfacl -R -m "u:$name:rwx" /$path
fi
  echo "${Red} Enter group name and path to directory or file: ${Endcolor}"
read path
	setfacl -R -m "g:$name:rwX" /$path
fi
  echo "${YELLOW}Finished. You should have access to the folders or file.${ENDCOLOR}"

exit 0
