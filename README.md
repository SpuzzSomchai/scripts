# Scripts

## Overview
Collection of scripts I have written or collected over the years. It will grow over time as I upload and find them across the various machines and clouds. I will break them down by PowerShell and Bash. If I feel really ambitious I'll break them down by use. I would check the code before running any (common sense) as some are kludgy at best or may point to specific resources.

## Acknowledgment
I have collected so many scripts that figuring out where they all came from is impossible. If I have included your work in this repo and you would like attribution or removed just notify me and I'll gladly remove it/them.

## Project status
On going! Never ending! Relentless. So on and so forth...

## License
This work is licensed under a Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
